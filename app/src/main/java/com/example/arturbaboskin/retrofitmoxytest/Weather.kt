package com.example.arturbaboskin.retrofitmoxytest

class Weather {
    var date: String = ""
    var tod: String = ""
    var temp: String = ""

    override fun toString(): String {
        return "$date | $tod | $temp"
    }
}