package com.example.arturbaboskin.retrofitmoxytest

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@InjectViewState 
class MainPresenter : MvpPresenter<MainView>() {

    fun load(tid: Int) {
        viewState.startLoad()
        val retrofit = Retrofit.Builder()
                .baseUrl("http://icomms.ru/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val service = retrofit.create(APIService::class.java)
        val call = service.getWeather(tid)
        call.enqueue(object : Callback<List<Weather>> {
            override fun onFailure(call: Call<List<Weather>>?, t: Throwable?) {
                viewState.onError()
            }

            override fun onResponse(call: Call<List<Weather>>?, response: Response<List<Weather>>?) {
                val data = response!!.body()
                viewState.stopLoad(data!!)
            }

        })

    }
}