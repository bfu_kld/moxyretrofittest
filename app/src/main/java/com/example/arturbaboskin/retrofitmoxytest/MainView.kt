package com.example.arturbaboskin.retrofitmoxytest

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun startLoad()

    @StateStrategyType(SingleStateStrategy::class)
    fun stopLoad(data: List<Weather>)

    @StateStrategyType(SingleStateStrategy::class)
    fun onError()
}