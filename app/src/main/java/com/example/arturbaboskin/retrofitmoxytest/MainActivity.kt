package com.example.arturbaboskin.retrofitmoxytest

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.ArrayAdapter
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null)
            presenter.load(24)

        swipeRefresh.setOnRefreshListener { presenter.load(24) }
    }

    override fun startLoad() {
        swipeRefresh.isRefreshing = true
    }

    override fun stopLoad(data: List<Weather>) {
        swipeRefresh.isRefreshing = false
        list!!.adapter = ArrayAdapter(applicationContext,
                android.R.layout.simple_list_item_1, data)
    }

    override fun onError() {
        swipeRefresh.isRefreshing = false
        Snackbar.make(list, "Что-то пошло не так :(", Snackbar.LENGTH_INDEFINITE)
                .setAction("повторить", { presenter.load(24) })
                .show()
    }
}
